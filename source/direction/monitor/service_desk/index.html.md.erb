## Service Desk

| | |
| --- | --- |
| Stage | [Monitor](/direction/monitor/) |
| Maturity              | [Viable](/direction/maturity/) |
| Documentation Link    | [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html#service-desk) |

Great products need to offer a great support experience. The GitLab Service Desk aims to be the primary medium which connects customers to product support staff.

Service Desk allows your organization the opportunity to provide an email address to your customers. These customers can send issues, feature requests, comments, and suggestions via email, with no external tools needed. These emails become issues right inside GitLab, potentially even in the same project where you are developing your product or service, pulling your customers directly into your DevOps process.

#### Intent

In an effort to clearly define a concrete and inspirational intent, it is important to answer this single question -- *"If Service Desk can be truly excellent at only one thing, what would it be?"* This is the intent of the Service Desk:

> To provide a conduit through which customers and support staff can **effectively collaborate** using **familiar process flows** to achieve  **prompt problem resolution**.

At GitLab, we don't really have a concept of `done`, but instead believe we should continue to iterate toward a more mature product as defined by our [maturity framework](https://about.gitlab.com/direction/maturity/). To better clarify our strategy, we must first understand what it will mean to have achieved Lovable Maturity. This is how we will know:

It is important to clearly define the desired user experience for a feature like the Service Desk. Not only do we desire to make providing support and issue resolution fluid and collaborative, but this feature can be exposed to the end-user. Ensuring that end-users receive exceptionally high quality communication is imperative to both us and our customers.

**Intuitive:** When users find issue or need support, communication should be as simple as possible -- ideally utilizing existing collaboration mechanisms. Currently, we support email integration in an effort to make requests as simple as sending an email. We're also exploring ways to integrate other communication channels such as Slack, to provide additional ways of reaching out to support teams. Since the Service Desk creates issues, the entire host of issue tracking and management tools can be utilized.

**Collaborative:** Sometimes it takes a team to resolve an end-user's problem. We're attempting to break down the silo surrounding help-desk requests and bring those issues into the existing issue tracking paradigm. Support Engineers can easily tag software developers, security analysts, or any other team members who can all share a single issue and therefore a single source of truth.

**Efficient:** Support is requested when features are missing or problems arise. This can be a stressful time, so it's important to ensure fast and accurate support interactions. Notes, comments, and any other internal / customer interactions should be easily available for all necessary parties. We are also looking for ways to provide support metrics, to track time to resolution as well as repeat issues.

**Intelligent:** We are looking to leverage [autonomation](https://en.wikipedia.org/wiki/Autonomation) to decrease human intervention and improve the accuracy of support interactions. We want to strive for a few manual steps as possible for categorization, triage, and other administrative tasks to allow the support team to spend more time providing valuable customer interactions and resolving issues. Moreover, we want to automate the routing of support tickets to the right team when they are created.

#### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

A primary goal is to make the Service Desk an integral part of the GitLab support workflow.

- We strive to [dogfood everything](/handbook/values/#dogfooding). We are in the process of interviewing and collaborating with our internal customers to make the Service Desk a much more widely utilized feature. Our hope is that through our own use, we will identify which additional features would allow the Service Desk to be a truly collaborative and efficient environment.

We also intend to make use of [on-call schedule management](https://gitlab.com/groups/gitlab-org/-/epics/3960) which is being built by the [Monitor:Health group](https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/). Once completed, that feature set will allow managers to put support teams on-call and to automate the routing of support tickets to the right team member.

#### Target Audience

The target audience for the Service Desk is as follows:

- Support Engineer - These engineers are the customer facing representatives of the business, and want to be able to efficiently resolve problems as they arise. They are frustrated by manual steps which divert their focus from solving real problems for the customers they serve, and strive to represent their company in the best way possible.
- Software Developers - Software developers may be called on by the Support Engineers to collaborate on customer issues. They want to be able to easily understand the issue, and provide timely feedback. They are frustrated by needing to work in a separate system as it delays collaboration and creates a disjointed workflow.

#### What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->
