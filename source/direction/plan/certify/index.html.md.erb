---
layout: markdown_page
title: "Category Direction - Certify"
description: View the group strategy for the Certify group, part of the plan stage.
canonical_path: "/direction/plan/certify/"
---

- TOC
{:toc}

## Certify Group

**Last Updated:** 2022-12-06

This is the direction page for the Certify Group, which is part of the [Plan stage](/direction/plan/) of the DevOps life cycle. Certify Group is responsible for the following categories:

| Category                | Direction | Description | Maturity |
| :---                    | :---      | :---        | :---:    |
| Requirements Management | See the [Requirements Management](#requirements-management) section | Manage functional requirements within GitLab | [minimal](/direction/maturity/) |
| Quality Management | See the [Quality Management](#quality-management) section | Manage and trace test cases within GitLab | [minimal](/direction/maturity/) |

### Team members

<%= stable_counterparts(role_regexp: /[,&] (Plan(?!:)|Plan:Certify)/) %>

### What are we working on and why

For [15.7](https://gitlab.com/gitlab-org/plan/-/issues/735) Certify Group is [deprecating and removing the existing dedicated requirement object](https://gitlab.com/groups/gitlab-org/-/epics/9203), as well as allowing [referencing of Requirements from test reports](https://gitlab.com/gitlab-org/gitlab/-/issues/329435). Additionally in 15.7, Certify Group is improving quality via resolution of two Service Desk Bugs, and performing UX Design work on Issues across Requirements Management, Quality Management, and Service Desk.

Certify covers a broad area, which requires focused prioritization. The following is a breakdown of our current level of effort spent on each category and why.

#### Requirements Management: Allocation: 85%

As discussed in our [certify goals](#current-certify-goals), we believe that we can make the largest positive impact to our customers by supporting requirement decomposition and traceability from within GitLab, so we are devoting the bulk of our resources to these areas.

Certify Group is focusing on [migrating Requirements functionality](https://gitlab.com/gitlab-org/plan/-/issues/735) from an independent object to a general Work Item type, which is part of a broader initiative in the Plan Stage to create greater continuity between Work Item types (such as Requirements, Issues, Epics, and Tasks).

#### Quality Management: Allocation 15%

In conjunction with Requirements Management, tracing requirements to test cases aligns closely with our vision to have work for software requirements down to test cases developed and tracked within GitLab.

### Current Certify Goals

The Certify Group aims to provide capabilities to allow requirements based development and testing within GitLab. The belief is that bringing requirements and traceability within GitLab will yield less context switching for our users, and provide increased productivity.

One of the most time consuming aspects of requirements based development is providing traceability between source code, testing, and design. This often requires switching between multiple tools, and manually inputting and updating traced artifacts during code updates and test runs. Our aim is to start with the [Software Developer](/handbook/product/personas/#sasha-software-developer) and [Software Engineering in Test](/handbook/product/personas/#simone-software-engineer-in-test) personas, with the goal of solving their immediate frustrations.

At present, both [Quality Management](#quality-management) and [Requirements Management](#requirements-management) categories are considered at [minimal maturity](/direction/maturity/).

It is our belief that solving the following three fundamental problems will bring both the Requirements Management and Quality Management capabilities to [viable maturity](/direction/maturity/).

#### Documenting requirements and test cases within GitLab

| **Status** | Complete - Both Requirements and test cases can be created and managed within GitLab |

It is our fundamental belief that for maximum efficiency and reduced cycle time, users should be able to complete their jobs to be done in a single cohesive application. This reduces the overall mental strain of switching between applications, but also allows for automation which can reduce manual steps.

#### Bi-directional linking between requirements, test cases and other artifacts

| **Status** | In Progress - We aim to build on top of the current [work item initiative](https://gitlab.com/groups/gitlab-org/-/epics/6033) provide bi-directional linking between requirements, test cases, and other work item types. |

The intention of requirements based development is to provide traceability between the requirements, and the other product artifacts such as code, design, and test cases. If this traceability is not provided within GitLab, then external tooling will be necessary which reduces productivity.

#### Allow testing to satisfy requirements

| **Status** | In Progress - It is possible to [satisfy requirements from automated CI/CD pipelines](https://docs.gitlab.com/ee/user/project/requirements/#allow-requirements-to-be-satisfied-from-a-ci-job) within GitLab. Please check out our [Walk-through of Requirements Traceability within GitLab](https://youtu.be/VIiuTQYFVa0). |

We would like to extend this functionality to provide additional methods for linking requirements to test cases.

### Certify Team Long term goals

Once Certify is a viable option for the [Software Developer](/handbook/product/personas/#sasha-software-developer) and [Software Engineering in Test](/handbook/product/personas/#simone-software-engineer-in-test) personas, we plan to continue iterating as follows:

- GitLab continues to support larger enterprises, and the natural need for multiple levels of requirements and test cases which can be decomposed down to requirements or test cases at lower levels has risen. Our objective is to expand our Work Item definition to allow for multi-level objects. This would allow for teams to create a system of sub-systems and perform all requirement tracing and test tracing directly within GitLab, further adhering to our mission as The One DevOps Platform.
- We recognize that requirements and their associated trace data is often required as release evidence / artifacts. We would like to work closely with our release team to integrate requirements traceability into release evidence.
- Visual representation of traceability and test coverage is also of importance. We would like to provide a visual representation of ancestors and descendants of requirements, making it easy to visualize decomposition and traceability. It would also be ideal for passing / failing test results to roll up visually to the requirements, allowing for quick visualization of the requirement status with regards to implementation and verification.

### Requirements Management

|                       |                                 |
| -                     | -                               |
| Maturity              | [Minimal](/direction/maturity/) |
| Documentation Link    | [Requirements Management](https://docs.gitlab.com/ee/user/project/requirements/) |

Requirements Management enables documenting, tracing, and control of changes to agreed-upon requirements in a system. Our strategy is to make it simple and intuitive to create and trace your requirements throughout the entire Software DevOps lifecycle.

We believe we can reduce the friction associated with managing requirements by tying it directly into the tools that a team uses to plan, create, integrate, and deploy their products. This can also provide real-time traceability and remove the need to track requirements across many disparate tools.

#### What is Requirements Management

It is often necessary to specify behaviors for a system or application. Requirements Management is a process by which these behaviors would be captured so that there is a clearly defined scope of work. A good general overview is provided in an [article from PMI](https://www.pmi.org/learning/library/requirements-management-planning-for-success-9669). For less restrictive environments, Requirements Management can take the form of jobs to be done (JTBD) statements, which are satisfied through iterative improvements or additional features.

Requirements management tools are often prescriptive in their process, requiring users to modify their workflows to include traceability. Our goal is to allow for such rigid process where required, but remove these barriers for organizations looking to achieve the process improvements offered by working with requirements in a less formal manner.

#### Aerospace Use Case

Regulated industries often have specific standards which define their development life-cycle. For example, commercial software-based aerospace systems must adhere to [RTCA DO-178C, Software Considerations in Airborne Systems and Equipment Certification](https://en.wikipedia.org/wiki/DO-178C). While this document covers all phases of the software development life cycle, the concept of traceability (defined as a documented connection) is utilized throughout. This connection must exist between the certification artifacts.

The most common trace paths needed are as follows:

- Software Allocated System Level Requirements <- High Level Software Requirements (HLR) <- Low Level Software Requirements (LLR) / Software Design <- Source Code <- Executable Object Code
- Software High Level & Low Level Requirements <- Test Cases <- Test Procedures <- Test Results

It is important to recognize that all artifacts must be under revision control.

During audits, teams are asked to demonstrate traceability from the customer specification through all downstream, version-controlled artifacts. Teams are often asked to analyze a change in a system level requirement, assessing exactly which downstream artifacts will need to be modified based on that change.

Further research has shown that many other regulated industries have similar process requirements, such as those in the medical, financial, and automative.

#### Key Terms / Concepts

**Traceability** - The ability to link requirements to other requirements (both higher level and lower level), design, source code, or verification tests.

**Requirements Decomposition** - It is up to the developers and architects to decompose (break down) high level requirements into many smaller low level requirements. All of these decomposed requirements would generally trace up to the high level requirement, thus forming a one-to-many (HLR to LLR) relationship.

**Derived Requirements** - Because regulated industries often require that all functionality within the software trace to a requirement, it is often necessary to create requirements at the LLR / Design level. These requirements, that were not decomposed from a higher level requirement, are called Derived Requirements.

**Traceability Matrix** - A common artifact that is often required is a traceability matrix. This is a released document which shows all traceability links in the system / sub-system.

#### Competitive landscape

Top competitors in this area include Jama Connect, Modern Requirements, and Rational DOORS by IBM . We believe that managing requirements within GitLab, rather than via an external tool integrated into the platform, can offer a much better user experience.

### Quality Management

|                       |                                 |
| -                     | -                               |
| Maturity              | [Planned](/direction/maturity/) |
| Documentation Link    | [Test Cases](https://docs.gitlab.com/ee/ci/test_cases/) |

Many organizations manage quality through both manual and automated testing. This testing is organized by test cases. These test cases can be run in different combinations and against different environments to create test sessions. Our goal for Quality management in GitLab is to allow for uses to track performance of test cases against their different environments over time, allowing for analysis of trends and identifying critical failures prior to releasing to production.

We have performed a Solution Validation for the [Quality Management MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/208306).

#### What's next & why

The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself. The MVC can be seen at [https://gitlab.com/groups/gitlab-org/-/epics/3852](https://gitlab.com/groups/gitlab-org/-/epics/3852).

Over the next year, the Plan stage will be focused on consolidating Issues, Requirements and Epics into Work Items.

#### Competitive landscape

Competitors in this space include qTest, Test Rail, and HPQC (HP Quality Center). They are focused on managing test cases as part of the software development lifecycle. Our approach and response will be to have similar basic test case management features (i.e. test objects), and then quickly move horizontally to integrate with other places in GitLab, such as issues and epics and even requirements management. See [this epic](https://gitlab.com/groups/gitlab-org/-/epics/670) for more information. With this strategy, we would not be necessarily competing directly with these existing incumbents, but helping users with the integration pains of multiple tools and leveraging other, more mature areas of GitLab as we iterate.



