---
layout: markdown_page
title: "Product Direction - Fulfillment: Platform"
description: "The Fulfillment Platform team at GitLab focuses on providing the foundations to our billing system."
canonical_path: "/direction/fulfillment/platform/"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
    
## Mission

> The Fulfillment Platform group provides foundational GitLab Order-To-Cash solutions. These solutions enable other fulfillment groups and contributors to ship with confidence, speed and efficiency, while ensuring that GitLab's fulfillment systems remain robust and performant.

## Overview

The Fulfillment Platform group owns, maintains and evolves the underlying architecture and orchestration for GitlLab’s order-to-cash flow (OTC). 

We refer to the architecture and orchestration of GitLab's order-to-cash flow as Fulfillment infrastructure.

In order to enable other groups, contributors, and stakeholders, we are focusing on 3 key areas of the Fulfillment infrastructure:

| Key area                      | Principle                                                                                                                 | How                | Performance Indicator |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------------|--------------------| ----------------- |
| System reliability            | Provide fulfillment infrastructure with best in class reliability and availability. | [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) has a `99.95%` target [availability and must stay within allowed error budgets](https://dashboards.gitlab.net/d/stage-groups-detail-fulfillment_platform/stage-groups-fulfillment-platform-group-error-budget-detail?orgId=1). | [General availability](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---general-availability-through-slis-and-error-budgets) and [Customer Journey Error Rate(s)](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---customer-journey-error-rates) |
| Developer productivity        | Provide a great experience for every developer that contributes to fulfillment solutions.                   | Make code contribution easier by maintaining the underlying architecture, keeping it performant, observable and decrease complexity through abstractions at the platform level. | [Mean time to recovery](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---mean-time-to-recovery-mttr) |
| Fulfillment architecture| Make fulfillment architecture and processes simple and digestible.               | Abstract integration complexity with Zuora and improve the architecture and orchestration of [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) to better align with the order-to-cash systems.  | [Data integrity](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---data-integrity) |

### CustomersDot

[CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) is the central application for the Fulfillment Platform group.

The group is responsible for the following areas of CustomersDot:

- Zuora integration
- Sign-up/Sign in/SSO
- OTC data architecture and orchestration in CustomersDot


## Performance indicators

We use performance indicators (PI) to track our progress. The current understanding of our group's northstart metric is [the general availability of our services](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---general-availability-through-slis-and-error-budgets). We aim to have at least 1 leading PI per key area (outlined above) that helps us stay on track with our group's mission. 

The status of these PI's is reviewed monthly with product leadership. For the latest metrics, see our [GitLab Internal Handbook section](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/).

## Target audience

As our responsibilities mostly span backend and architectural work, our direct target audience is GitLab internally focused:
    
- Other fulfillment groups and team members
- Internal stakeholders (Customer Success, Sales, Billing)

We improve the experience of all of our customers (self-service, sales-assisted, and reseller customers) by improving fulfillment systems' process flows and subscription data accuracy.

## What's up now ( up to 12 months)

Within the next 12 months we want to strengthen the foundation of the fulfillment platform. Our main focus is to improve our order-to-cash systems and underlying data architecture, which will help us scale as GitLab continues to grow.

**Projects for key areas:**

- Fulfillment architecture: [Introduce and enable a CustomersDot Billing Account](https://gitlab.com/groups/gitlab-org/-/epics/8331)
- Developer productivity: [Upgrade to Zuora Orders API](https://gitlab.com/groups/gitlab-org/-/epics/6438)
- Fulfillment architecture: [GitLab.com SSO as the only login option for CustomersDot to reduce data discrepency](https://gitlab.com/groups/gitlab-org/-/epics/8905)
- System reliability: [Increase level maturity for CustomersDot](https://about.gitlab.com/handbook/engineering/infrastructure/service-maturity-model/#customersdot-detail)

**Roadmap:**

- For a full list of our upcoming and ongoing projects, check out our [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Afulfillment+platform&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP). 
- We also have a `Fulfillment Roadmap by Group` slide deck (Not Public), that internal team members can reference for executive reviews.

### InfraDev work 

As one of our key areas spans developer productivity we  work on performance, scalability, and observability efforts with help from the [Infrastructure department](https://about.gitlab.com/handbook/engineering/infrastructure/).

We want to increase the [level maturity of CustomersDot](https://about.gitlab.com/handbook/engineering/infrastructure/service-maturity-model/#customersdot-detail) and continue improving our availability and observability through the following projects:

| What | Why | When |
|----- |----- |----- |
| [Improve redundancy between CustomersDot and Zuora ](https://gitlab.com/groups/gitlab-org/-/epics/8850) | Adds a better caching layer to have no downtime due to frequent Zuora outages | Next (3-6 milestones) |
| [Iteratively migrate CustomersDot to a HA environment](https://gitlab.com/groups/gitlab-org/-/epics/9278) | Move CustomersDot services/components to high availability in order to improve reliability | Next (3-6 milestones) |
| [Implement auto-rollback on error](https://gitlab.com/gitlab-org/customersdot-ansible/-/issues/156) | Minimize disruption if an outage occurs after a deployment | Later (6-12 milestones) |
| Alerting of failed jobs for critical [SaaS](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/808) and [SM](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/809) metrics | Move away from noisy Sentry alerts to critical alerts for accurate SaaS and SM billing | Later (6-12 milestones) |
| [Alerting over a threshold of payment failures](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4145) | Encounter payment problems as they occur | Later (6-12 milestones)  |

## Possible future oportunities

The further we move along with our current key projects for foundational strength, we will identify new areas of opportunity that focus on systems reliability, developer productivity and Fulfillment process efficiency.
    
__This can entail:__

- Further refinement of our [Order-2-Cash Systems Data Architecture](https://gitlab.com/gitlab-org/customers-gitlab-com/-/blob/main/doc/architecture/data_architecture_proposal.md)
- Consolidated logging of errors in one tool of choice
- Further strengthening of Zuora as SSOT
- Enabling other Fulfillment groups for specific projects


## Key Links

- [Performance indicators](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/#fulfillment-platform---general-availability-through-slis-and-error-budgets)
- [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Afulfillment+platform&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP)
